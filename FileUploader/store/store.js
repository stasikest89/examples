import React from 'react'
import FileUploadContext from './context'

export const useStore = () => React.useContext(FileUploadContext)
export const StoreProvider = ({ children, initialState, reducer }) => {
	const [globalState, dispatch] = React.useReducer(reducer, initialState)

	return (
		<FileUploadContext.Provider value={[globalState, dispatch]}>
			{children}
		</FileUploadContext.Provider>
	)
}