export const SET_UPLOAD_FILE = 'SET_UPLOAD_FILE'
export const SET_GLOBAL_NOTIFICATION = 'SET_GLOBAL_NOTIFICATION'
export const SET_RESULT = 'SET_RESULT'
export const SET_UNSUPPORTED_HEADERS = 'SET_UNSUPPORTED_HEADERS'
export const SET_VALIDATION_FINISHED = 'SET_VALIDATION_FINISHED'
export const SET_FILE_TO_UPLOAD = 'SET_FILE_TO_UPLOAD'
export const SET_DEFAULT_STATE = 'SET_DEFAULT_STATE'
export const SET_WEBSOCKET = 'SET_WEBSOCKET'

export const initialState = {
	uploadingFile: {},
	unsupportedHeaders: [],
	validationFinished: false,
	globalNotification: {},
	fileToUpload: {},
	result: 'success',
	webSocket: {}
};

export const setUploadFile = (uploadingFile) => ({
	type: SET_UPLOAD_FILE,
	uploadingFile
});

export const setGlobalNotification = (globalNotification) => ({
	type: SET_GLOBAL_NOTIFICATION,
	globalNotification
});

export const setResult = (result) => ({
	type: SET_RESULT,
	result
});

export const setUnsupportedHeaders = (unsupportedHeaders) => ({
	type: SET_UNSUPPORTED_HEADERS,
	unsupportedHeaders
});

export const setValidationFinished = (validationFinished) => ({
	type: SET_VALIDATION_FINISHED,
	validationFinished
});

export const setFileToUpload = (fileToUpload) => ({
	type: SET_FILE_TO_UPLOAD,
	fileToUpload
});

export const setDefaultState = () => ({
	type: SET_DEFAULT_STATE
});

export const setWebSocket = (webSocket) => ({
	type: SET_WEBSOCKET,
	webSocket
});

export const uploadFileReducer = (state = initialState, action) => {
	if (action.type === SET_UPLOAD_FILE) {
		return {
			...state,
			uploadingFile: action.uploadingFile
		}
	}

	if (action.type === SET_GLOBAL_NOTIFICATION) {
		return {
			...state,
			globalNotification: action.globalNotification
		}
	}

	if (action.type === SET_RESULT) {
		return {
			...state,
			result: action.result
		}
	}

	if (action.type === SET_UNSUPPORTED_HEADERS) {
		return {
			...state,
			unsupportedHeaders: action.unsupportedHeaders
		}
	}

	if (action.type === SET_VALIDATION_FINISHED) {
		return {
			...state,
			validationFinished: action.validationFinished
		}
	}

	if (action.type === SET_FILE_TO_UPLOAD) {
		return {
			...state,
			fileToUpload: action.fileToUpload
		}
	}

	if (action.type === SET_WEBSOCKET) {
		return {
			...state,
			webSocket: action.webSocket
		}
	}

	if (action.type === SET_DEFAULT_STATE) {
		return {
			...state,
			...initialState
		}
	}
};