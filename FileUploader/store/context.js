import React from 'react'

const FileUploadContext = React.createContext()

export default FileUploadContext;