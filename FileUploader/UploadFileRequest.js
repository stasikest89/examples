import React from 'react'
import BorderRadius from '../BorderRadius'
import Settings from '../Settings'
import './styles/_upload.scss'
import LinearProgress from '@material-ui/core/LinearProgress';

const UploadingFile = props => {
	const {
		file = {}
	} = props

	const {
		format,
		sourceHeader = "Source File",
		sourceMessage,
		percentage
	} = file

	return (
		<BorderRadius>
			<div 
				className="meta-file-uploader--container file-upload"
				style={{ background: Settings.colors.grey.lightest }}>

				<img 
					src={`${Settings.svg}/${format}.svg`}
					className="uploading-file"
				/>

				<div className="file-upload-content">
					<span className="source-file">
						{sourceHeader}
					</span>

					<div className="source-container">
						<span className="source">
							{sourceMessage}
						</span>

						<span className="uploading">Uploading</span>

						<img 
							src={`${Settings.svg}/clear-black.svg`} 
							className="clear"
						/>
					</div>

					<LinearProgress 
						variant="determinate" 
						value={percentage} 
						style={{
							marginTop: '0.3rem',
							background: '#d0d0d0'
						}}
						color="primary"
						classes={{
							barColorPrimary: 'meta-progess-bar'
						}}
					/>
				</div>

			</div>
		</BorderRadius>
	)
}

export default UploadingFile