import UploadingFile from './UploadingFile'
import UnsupportedHeaders from './UnsupportedHeaders'
import DefaultFileUploader from './DefaultFileUploader'

export default {
	DefaultFileUploader,
	UploadingFile,
	UnsupportedHeaders
}