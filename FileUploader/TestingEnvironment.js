import React from 'react'

import { useStore } from './store/store'

import { 
	setUploadFile, 
	setNotification, 
	setResult, 
	setUnsupportedHeaders, 
	setValidationFinished, 
	setFileToUpload 
} from './store/reducer'

import ValidationUpdateResponseTransformer from './src/ValidationUpdateResponseTransformer'

const reducer = {
	setUploadFile, 
	setNotification, 
	setResult, 
	setUnsupportedHeaders, 
	setValidationFinished, 
	setFileToUpload 
}

const TestingEnvironment = props => {
	const [, dispatch] = useStore()
	const [state] = useStore()

	const {
		fileToUpload = {}
	} = state

	const mockUpdateFile = () => {
		ValidationUpdateResponseTransformer({
			response: process.TEST_SET_UPLOAD_FILE,
			fileToUpload,
			dispatch,
			reducer,
			state
		})
	}

	const mockValidationFinished = () => {
		dispatch(
			setValidationFinished(true)
		)
	}

	return (
		<div style={{ display: 'none' }}>
			<div id="mockUpdateFile" onClick={() => mockUpdateFile()}></div>
			<div id="mockValidationFinished" onClick={() => mockValidationFinished()}></div>
		</div>
	)
}

export default TestingEnvironment