import React, { useState, useEffect } from 'react'
import { useStore } from './store/store'
import Notification from '../Notification'
import _ from 'lodash'

import { 
	setGlobalNotification
} from './store/reducer'

const NotificationComponent = props => {
	const [notification, setNotification] = useState({})
	const [unsupportedHeadersSet, setUnsupportedHeadersSet] = useState(false)
	const [state] = useStore()
	const [, dispatch] = useStore()

    const { 
		validationFinished,
		unsupportedHeaders = [],
		globalNotification = {}
	} = state

	const notificationToUse = !_.isEmpty(globalNotification) ? globalNotification : notification

	useEffect(() => {
		if(validationFinished && !_.isEmpty(unsupportedHeaders) && _.isEmpty(notification) && !unsupportedHeadersSet) {
			setNotification({
				title: 'Unsupported Headers',
				description: 'Sorry, your CSV has unsupported headers and cannot beingested. Please correct the headers listed in the Unsupported Header Log and try again.',
				status: 'error'
			})

			setUnsupportedHeadersSet(true)
		}

		if(validationFinished && _.isEmpty(unsupportedHeaders) && _.isEmpty(notification) && !unsupportedHeadersSet) {
			setNotification({
				title: 'File Submitted for Ingest',
				description: 'Your CSV has been checked and all headers are valid.The file has now been passed through for ingest into the Meta Title database.',
				status: 'success'
			})

			setUnsupportedHeadersSet(true)
		}
	})

	return (
		<Notification
			{...notificationToUse}
			intercationStatus={notificationToUse.status}
			onClick={() => {
				setNotification({})
				dispatch(setGlobalNotification({}))
			}}
		/>
	)
}

export default NotificationComponent