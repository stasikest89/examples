export default function SetUploadingProgress(props) {
	const {
		dispatch = () => {},
		reducer = {},
		uploadingFileState
	} = props 

	const {
		setUploadFile
	} = reducer

	let percentage = 0

	const timer = setInterval(() => {
		const diff = Math.random() * 10
		percentage = Math.min(percentage + diff, 100)

		if(percentage >= 100 || uploadingFileState.renderingFile) {
			clearInterval(timer)
		}
		
		dispatch(
			setUploadFile({
				...uploadingFileState,
				percentage
			})
		)
	}, 25)
}