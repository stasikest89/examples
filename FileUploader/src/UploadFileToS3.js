import axios from 'axios'
import SetUploadingProgress from './SetUploadingProgress'

export default function UploadFileToS3(props) {
	const {
		uploadUrl = "",
		fileToUpload = {},
		dispatch = () => {},
		reducer = {}
	} = props 

	const {
		setUploadFile,
		setNotification
	} = reducer

	const {
		name = ""
	} = fileToUpload

	axios({
		url: uploadUrl,
		method: 'PUT',
		data: fileToUpload
	}).then(response => {
		if(response.status == 200) {
			SetUploadingProgress(props)
		}
	})
	.catch(error => {
		console.log(error)
		
		dispatch(
			setNotification({
				title: 'Technical Error',
				description: `Failed to upload ${name} file. Please try again.`,
				status: 'error'
			})
		)

		dispatch(
			setUploadFile({})
		)
	})
}