import deepcopy from 'deepcopy'

export default function ValidationUpdateResponseTransformer(props) {
	const {
		response = {},
		fileToUpload = {},
		dispatch = () => {},
		reducer = {},
		state = {}
	} = props 

	const {
		setUploadFile,
		setResult,
		setUnsupportedHeaders
	} = reducer

	const {
		validationUpdate = {}
	} = response

	const {
		type,
		result,
		error,
		progress = {},
		header
	} = validationUpdate

	const {
		total,
		current
	} = progress
	
	const {
		name = ""
	} = fileToUpload

	const { 
		unsupportedHeaders = [],
	} = state

	const resultSuccess = "success"
	const resultError = "error"

	let uploadingFileState = {
		renderingFile: true,
		sourceHeader: name,
		sourceMessage: header || name,
		progress
	}

	if(current && total) {
		uploadingFileState.percentage = (current / total) * 100

		if(type) {
			uploadingFileState.uploadingMessage = `Validating ${type} ${current} of ${total}`
		}
	}

	if(result == resultSuccess) {
		dispatch(
			setUploadFile(uploadingFileState)
		)
	}

	if(result == resultError) {
		dispatch(
			setResult(resultError)
		)

		let updatedUnSupportedHeaders = deepcopy(unsupportedHeaders)

		updatedUnSupportedHeaders.push({
			column: current,
			header,
			message: error
		})
		
		dispatch(
			setUploadFile(uploadingFileState)
		)

		dispatch(
			setUnsupportedHeaders(updatedUnSupportedHeaders)
		)
	}
}