import React, { useEffect } from 'react'
import _ from 'lodash'
import './styles/_default.scss'
import Settings from '../Settings'
import BorderRadius from '../BorderRadius'
import Paragraph from '../Paragraph'
import ButtonFileUpload from '../Button/buttonFileUpload'
import { envTest } from '../Environment'
import WebSocket from '../WebSocket'
const dragDrop = require('drag-drop')
import $ from 'jquery'
import { useStore } from './store/store'
import { 
	setUploadFile, 
	setGlobalNotification, 
	setResult, 
	setUnsupportedHeaders, 
	setValidationFinished, 
	setFileToUpload,
	setWebSocket
} from './store/reducer'

import ValidationUpdateResponseTransformer from './src/ValidationUpdateResponseTransformer'
import UploadFileToS3 from './src/UploadFileToS3'

const DefaultFileUploader = props => {
	const {
		message,
		acceptedFormats
	} = props 

	const [, dispatch] = useStore();
	const [state] = useStore()

	const { 
		uploadingFile = {}
	} = state

	let fileToUpload = {}
	let uploadingFileState = {}

	if(!_.isEmpty(uploadingFile)) {
		return false
	}

	const reducer = {
		setUploadFile, 
		setGlobalNotification, 
		setResult, 
		setUnsupportedHeaders, 
		setValidationFinished, 
		setFileToUpload 
	}

	const uploadUrlAction = "get-upload-url"
	const importAction = "import"
	const validationUpdateAction = "validation-update"

	const handleUpload = event => {
		const {
			cmsOptions = {}
		} = props
	
		const {
			dataIngest,
			user_token
		} = cmsOptions
	
		const {
			environment = ""
		} = dataIngest
	
		const {
			target = {}
		} = event
	
		const {
			files = []
		} = target
	
		fileToUpload = files[0]

		if(envTest && process.TEST_SET_UPLOAD_FILE) {
			fileToUpload = process.TEST_SET_UPLOAD_FILE
		}
	
		if(fileToUpload) {
			const {
				name = ""
			} = fileToUpload
	
			dispatch(
				setFileToUpload(fileToUpload)
			)

			uploadingFileState = {
				sourceMessage: name
			}

			dispatch(
				setUploadFile(uploadingFileState)
			)

			if(!envTest) {
				WebSocket({
					url: `${process.env.WSS_FILE_UPLOAD_API_URL}/${environment}?token=${user_token}`,
					onOpen: webSocket => {
						webSocket.sendAction({
							action: {
								action: "get-upload-url",
								filename: name
							},
							onMessage: response => {
								if(response) {
									renderMessageResponse(response)
								}
							},
							onClose: () => console.log('closed'),
							onError: () => {
								dispatch(
									setGlobalNotification({
										title: 'Technical Error',
										description: `Failed to fetch url to upload. Please try again.`,
										status: 'error'
									})
								)
							}
						})

						dispatch(
							setWebSocket(webSocket)
						)
					}
				})
			}

			if(envTest) {
				dispatch(
					setUploadFile(uploadingFileState)
				)
			}
		} else {
			dispatch(
				setGlobalNotification({
					title: 'Technical Error',
					description: `Failed to select file to upload. Please try again.`,
					status: 'error'
				})
			)
		}
	}

	const renderMessageResponse = response => {
        const {
            uploadUrl = "",
            action
		} = response
		
		const commonProps = {
			dispatch,
			reducer,
			state,
			uploadingFileState
		}

        switch(action) {
            case uploadUrlAction:
                UploadFileToS3({
					uploadUrl,
					fileToUpload,
					...commonProps
				})
				
            	break;

            case validationUpdateAction:
				ValidationUpdateResponseTransformer({
					response,
					fileToUpload,
					...commonProps
				})

				break;

            case importAction:
				dispatch(
					setValidationFinished(true)
				)
        }
	}

	useEffect(() => {
		const target = "#dropTarget"

		if($(target) && !envTest) {
			dragDrop(target, {
				onDrop: (files) => {
					handleUpload({
						target: {
							files
						}
					})
				},
				onDragEnter: () => {
					$(target).css({
						background: Settings.colors.grey.light
					})
				},
				onDragLeave: () => {
					$(target).css({
						background: Settings.colors.grey.lightest
					})
				}
			})
		}
	}, [])

	return (
		<BorderRadius>
			<div 
				className="meta-file-uploader meta-file-uploader--container"
				id="dropTarget"
				style={{ background: Settings.colors.grey.lightest }}>

				<img src={`${Settings.images.path}/svg/cloud-computing-grey.svg`} />

				<ButtonFileUpload 
					{...Settings.components.actionButtonLarge.container}
					value="Browse Files"
					blue={true}
					margin="0.59375rem 0"
					onClick={() => {}}
					acceptedFormats={acceptedFormats}
					handleChange={e => handleUpload(e)}
				/>

				{message &&
					<Paragraph
						textAlign="center"
						color={Settings.colors.grey.dark}
						margin="0 auto"
						items={[message]}
						fontWeight={500}
						fontSize="0.78125rem"
					/>
				}
			</div>
		</BorderRadius>
	)
}

export default DefaultFileUploader