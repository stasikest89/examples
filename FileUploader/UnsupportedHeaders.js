import React from 'react'
import BorderRadius from '../BorderRadius'
import BasicRow from '../RecordTable/BasicRow'
import Tag from '../Tag'
import Settings from '../Settings'
import './styles/_upload.scss'
import { useStore } from './store/store'

const UnsupportedHeaders = props => {
	const [state] = useStore()

    const { 
		unsupportedHeaders
	} = state

	const RenderContent = item => {
		const {
			column = "",
			header = "",
			message = ""
		} = item 

		return (
			<div>
				<Tag
					text={`Column ${column}`}
					background={Settings.colors.red.default}
				/>

				<span className="meta-unsupported-files--text">
					Header named <b>{header}</b> {message}
				</span>
			</div>
		)
	}

	return (
		<BorderRadius 
			styles={{
				boxShadow: "0px 0.0625rem 0.0625rem 0 rgba(0, 0, 0, 0.24)"
			}}>

			<div className="meta-unsupported-files">
				{unsupportedHeaders.map((item, index) => (
					<BasicRow 
						index={index}
						even={index % 2 == 0}
						key={index}
						content={<RenderContent {...item}/>}
					/>
				))}
			</div>
		</BorderRadius>
	)
}

export default UnsupportedHeaders