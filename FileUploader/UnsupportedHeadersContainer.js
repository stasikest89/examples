import React, { Fragment } from 'react'
import _ from 'lodash'
import TitleContentH2 from '../Boilerplates/TitleContentH2'
import UnsupportedHeaders from '../FileUploader/UnsupportedHeaders'
import Button from '../Button'
import './styles/_upload.scss'
import { useStore } from './store/store'
import { setDefaultState } from './store/reducer'

const UnsupportedHeadersContainer = props => {
	const [state] = useStore()
	const [, dispatch] = useStore()

    const { 
		validationFinished,
		unsupportedHeaders
	} = state

	if(_.isEmpty(unsupportedHeaders)) {
		return false
	}

	return (
		<Fragment>
			<TitleContentH2 
				title="Unsupported Header Log"
				margin="1.5625rem 0 1.09375rem 0"
			/>

			<UnsupportedHeaders />

			{validationFinished &&
				<Button
					value="Clear Upload"
					margin="1.65625rem 0 0"
					padding="0.59375rem 1.0625rem"
					color="white"
					onClick={() => dispatch(setDefaultState())}
					grey={true}
					className="clear-upload"
				/>
			}
		</Fragment>
	)
}

export default UnsupportedHeadersContainer