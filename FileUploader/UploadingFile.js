import React, { useRef, useEffect } from 'react'
import _ from 'lodash'
import BorderRadius from '../BorderRadius'
import Settings from '../Settings'
import './styles/_upload.scss'
import LinearProgress from '@material-ui/core/LinearProgress';
import { useStore } from './store/store'

import { 
	setGlobalNotification, 
	setDefaultState
} from './store/reducer'

const UploadingFile = props => {
	const {
		format
	} = props

	const [state] = useStore()
	const [, dispatch] = useStore()
	const stateRef = useRef(state)

	useEffect(
		() => {
		  stateRef.current = state
		},
		[state],
	);

	const { 
		uploadingFile = {},
		result,
		validationFinished,
		webSocket,
		fileToUpload = {}
	} = state

	if(_.isEmpty(uploadingFile)) {
		return false
	}

	const {
		sourceHeader = "Source File",
		sourceMessage,
		percentage,
		uploadingMessage = "Uploading",
		progress
	} = uploadingFile

	let barColorPrimary = "meta-progess-bar"
	if(result) barColorPrimary = `meta-progess-bar-${result}`

	const handleClearUpload = () => {
		if(stateRef.current.validationFinished) {
			return dispatch(
				setGlobalNotification({
					title: 'Validation Finished',
					description: `Upload cannot be canceled once validation has been completed.`,
					status: 'alert'
				})
			)
		}

		webSocket.sendAction({
			action: {
				action: "cancel-upload",
				newFilename: fileToUpload.name
			},
			onError: () => {
				dispatch(
					setGlobalNotification({
						title: 'Technical Error',
						description: `Failed to clear upload. Please try again.`,
						status: 'error'
					})
				)
			}
		})

		dispatch(setGlobalNotification({}))
		dispatch(setDefaultState())
	}

	const clearUpload = () => {
		if(stateRef.current.validationFinished) {
			return 
		}

		dispatch(
			setGlobalNotification({
				title: 'Cancel Ingest?',
				description: `If you cancel this upload, the process will be abortedand the file will be deleted from the Meta application. Are you sure you want to cancel?`,
				status: 'alert',
				okText: 'Cancel Ingest',
				cancelText: 'Continue',
				confirm: () => handleClearUpload()
			})
		)
	}

	return (
		<BorderRadius>
			<div 
				className="meta-file-uploader--container file-upload"
				style={{ background: Settings.colors.grey.lightest }}>

				<img 
					src={`${Settings.svg}/${format}.svg`}
					className="uploading-file"
				/>

				<div className="file-upload-content">
					<span className="source-file">
						{sourceHeader}
					</span>

					<div className="source-container">
						<span className="source">
							{sourceMessage}
						</span>

						<span className="uploading">
							{validationFinished ? 'Validation Complete' : uploadingMessage}
						</span>

						{!_.isEmpty(progress) &&
							<img 
								src={`${Settings.svg}/${validationFinished ? 'tick-grey' : 'clear-black'}.svg`}
								className="clear-tick"
								onClick={() => clearUpload()}
							/>
						}
					</div>

					<LinearProgress 
						variant="determinate" 
						value={percentage || 0} 
						style={{
							marginTop: '0.3rem',
							background: '#d0d0d0'
						}}
						color="primary"
						classes={{
							barColorPrimary
						}}
					/>
				</div>

			</div>
		</BorderRadius>
	)
}

export default UploadingFile