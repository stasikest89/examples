import React, { useState, useEffect, useRef } from 'react'
import $ from 'jquery'

const FilterContentContainer = props => {
	const {
		styles = {},
		type,
		overflow = true
	} = props

	const divRef = useRef(null)

	useEffect(() => {
		if(window) {
			const parentFilter = $(`#filter--${type}`).last()
			const windowWidth = window.innerWidth
			const div = divRef.current
			const viewportOffset = div.getBoundingClientRect()
			const divToTheRight = windowWidth - viewportOffset.right
			const divToTheLeft = windowWidth - viewportOffset.left
			const parentOffset = parentFilter.position() || {}
			const parentHeight = (parentFilter.height() || 0) + 12

			let style = {
				left: (parentOffset.left / 16) + 'rem',
				top: ((parentOffset.top + parentHeight) / 16) + 'rem'
			}

			if(divToTheRight < 0) {
				style = {
					left: 'auto',
					right: 0
				}
			}

			if(divToTheLeft < 0) {
				style = {
					left: 0,
					right: 'auto'
				}
			}

			$(div).css(style)
		}
    })

	return (
		<div 
			className="search-and-filter--content-container"
			ref={divRef}
			style={styles}>

			<div className={`content-container--content ${overflow ? 'content-overflow' : ''}`}>
				{props.children}
			</div>
		</div>
	)
}

export default FilterContentContainer