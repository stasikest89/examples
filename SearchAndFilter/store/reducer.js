import filtersApplied from './filtersApplied'

export const SET_TITLE_SEARCH = 'SET_TITLE_SEARCH'
export const SET_GENRES = 'SET_GENRES'
export const SET_RELEASE_YEAR = 'SET_RELEASE_YEAR'
export const SET_CERTIFICATIONS = 'SET_CERTIFICATIONS'
export const SET_IMDB_RATING = 'SET_IMDB_RATING'
export const SET_AUDIO_LANGUAGES = 'SET_AUDIO_LANGUAGES'
export const SET_SUBTITLES = 'SET_SUBTITLES'
export const SET_ACTIVE_FILTER = 'SET_ACTIVE_FILTER'
export const SET_DEFAULT_STATE = 'SET_DEFAULT_STATE'

export const filtersKeys = [
	'titleSearch',
	'genres',
	'releaseYear',
	'certifications',
	'imdbRating',
	'audioLanguages',
	'subtitles'
]

export const apiKeys = [
	'items',
	'condition'
]

export const defaultFilters = {
	titleSearch: "",
	genres: {
		items: [],
		condition: 'and',
		useValue: true
	},
	releaseYear: {
		items: [],
		condition: 'is',
		joiningConditions: {
			is: 'or',
			from: 'to'
		},
		prefixCondition: true
	},
	certifications: {
		items: [],
		condition: 'and'
	},
	imdbRating: {
		items: [],
		condition: 'or above',
		postFix: '+'
	},
	audioLanguages: {
		items: [],
		condition: 'and',
		useValue: true,
		prefix: 'Dubs available in',
		joiningCondition: '|'
	},
	subtitles: {
		items: [],
		condition: 'and',
		useValue: true,
		prefix: 'Subtitles available in',
		joiningCondition: '|'
	}
}

export const initialState = {
	...defaultFilters,
	activeFilter: "",
	filtersApplied: false,
	viewKey: 'search-filter-1'
};

export const setTitleSearch = (titleSearch) => ({
	type: SET_TITLE_SEARCH,
	titleSearch
});

export const setGenres = (genres) => ({
	type: SET_GENRES,
	genres
});

export const setReleaseYear = (releaseYear) => ({
	type: SET_RELEASE_YEAR,
	releaseYear
});

export const setCertifications = (certifications) => ({
	type: SET_CERTIFICATIONS,
	certifications
});

export const setIMDBRating = (imdbRating) => ({
	type: SET_IMDB_RATING,
	imdbRating
});

export const setAudioLanguages = (audioLanguages) => ({
	type: SET_AUDIO_LANGUAGES,
	audioLanguages
});

export const setSubtitles = (subtitles) => ({
	type: SET_SUBTITLES,
	subtitles
});

export const setActiveFilter = (activeFilter) => ({
	type: SET_ACTIVE_FILTER,
	activeFilter
});

export const setDefaultState = () => ({
	type: SET_DEFAULT_STATE
});

export const searchAndFilterReducer = (state = initialState, action) => {
	let updatedState = {
		...state
	}

	if (action.type === SET_TITLE_SEARCH) {
		updatedState.titleSearch = action.titleSearch
	}

	if (action.type === SET_GENRES) {
		updatedState.genres = action.genres
	}

	if (action.type === SET_RELEASE_YEAR) {
		updatedState.releaseYear = action.releaseYear
	}

	if (action.type === SET_CERTIFICATIONS) {
		updatedState.certifications = action.certifications
	}

	if (action.type === SET_IMDB_RATING) {
		updatedState.imdbRating = action.imdbRating
	}

	if (action.type === SET_AUDIO_LANGUAGES) {
		updatedState.audioLanguages = action.audioLanguages
	}

	if (action.type === SET_SUBTITLES) {
		updatedState.subtitles = action.subtitles
	}

	if (action.type === SET_ACTIVE_FILTER) {
		updatedState.activeFilter = action.activeFilter
	}

	if (action.type === SET_DEFAULT_STATE) {
		let newViewKey = `${updatedState.viewKey}1`
		updatedState = initialState
		updatedState.viewKey = newViewKey
	}

	updatedState.filtersApplied = filtersApplied(updatedState)

	return updatedState
};