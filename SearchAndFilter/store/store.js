import React from 'react'
import SearchAndFilterContext from './context'

export const useStore = () => React.useContext(SearchAndFilterContext)
export const StoreProvider = ({ children, initialState, reducer }) => {
	const [globalState, dispatch] = React.useReducer(reducer, initialState)

	return (
		<SearchAndFilterContext.Provider value={[globalState, dispatch]}>
			{children}
		</SearchAndFilterContext.Provider>
	)
}