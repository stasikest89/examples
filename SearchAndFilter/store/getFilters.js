import { filtersKeys, apiKeys } from './reducer'

export default function getFilters(props) {
	const {
		state = {}
	} = props

	let updatedState = {}
	filtersKeys.forEach(filter => {
		const filterItem = state[filter]
		let filterResult = {}

		if(filterItem) {

			if(typeof filterItem == 'object') {
				apiKeys.forEach(apiKey => {
					filterResult[apiKey] = filterItem[apiKey]
				})

				updatedState[filter] = filterResult
			}

			if(typeof filterItem == 'string') {
				updatedState[filter] = filterItem
			}
		}
	})
	
	return updatedState
}