import React from 'react'

const SearchAndFilterContext = React.createContext()

export default SearchAndFilterContext;