import { defaultFilters } from './reducer'
import _ from 'lodash'

export default function filtersApplied(state) {
    let result = false 

    Object.keys(defaultFilters).forEach(filter => {
        const item = state[filter]

        if(typeof item == "string" && !_.isEmpty(item)) {
            result = true
        }

        if(item && item.items && !_.isEmpty(item.items)) {
            result = true
        }
    })

    return result
}