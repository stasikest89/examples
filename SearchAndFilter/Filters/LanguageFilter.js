import React, { useState } from 'react'
import _ from 'lodash'
import FilterContentContainer from '../FilterContentContainer'
import Filter from './Filter'
import ConnectSearchWithLabels from '../../SearchBox/ConnectSearchWithLabels'
import { useStore } from '../store/store'
import { ADD_SELECTION, DELETE_SELECTION, SET_CONDITION, updateState } from '../src/handleUpdateState'
import StringUnderlineSearch from '../../Helpers/StringUnderlineSearch'
import ClientManagedLanguages from '../../ClientManagedOptions/languages'

const LanguageFilter = props => {
	const {
		languageType,
		reducer,
		options,
		spoken,
		cmsData = {}
	} = props

	const {
		clientFeatures = {}
	} = cmsData

	const [state] = useStore()
	const [, dispatch] = useStore()

	const {
		activeFilter,
	} = state

	const languageState = state[languageType] || {}

	const {
		items = [],
		condition
	} = languageState

	const {
		spoken_languages = [],
		all_written_languages = []
	} = options
	
	const active = activeFilter == languageType
	const languages = ClientManagedLanguages({
		options,
		spoken,
		fallback: spoken ? spoken_languages : all_written_languages,
		clientFeatures,
		languageKey: spoken ? 'dubbing_tracks' : 'subtitles' 
	})

	const [list, setList] = useState(languages)

	function getList(search) {
		if(Array.isArray(languages)) {
			return languages.map(item => {
				const value = item.text || ""
	
				return {
					text: StringUnderlineSearch(value, search),
					value
				}
			})
		}

		return []
	}

	const handleSearch = value => {
		const listOptions = getList(value)

		setList(
			listOptions.filter(item => item.text && item.text.toLowerCase().includes(value.toLowerCase()))
		)
	}

	const changeProps = {
		state,
		type: languageType,
		dispatch,
		reducer
	}

	return (
		<Filter {...props}>
			{active &&
				<FilterContentContainer 
					type={languageType}
					styles={{ width: 'max-content' }}>
						
					<ConnectSearchWithLabels 
						handleSearch={handleSearch}
						placeholder="Select Language"
						containerStyles={{
							minWidth: '13.5rem',
						}}
						list={list}
						items={items}
						condition={condition}
						displayAndOr={Array.isArray(items) && items.length > 1}
						handleSelection={item => {
							updateState({
								item,
								...changeProps,
								action: ADD_SELECTION
							})
						}}
						handleDelete={item => updateState({
							item,
							...changeProps,
							action: DELETE_SELECTION
						})}
						handleAndClick={() => updateState({
							...changeProps,
							action: SET_CONDITION,
							condition: 'and'
						})}
						handleOrClick={() => updateState({
							...changeProps,
							action: SET_CONDITION,
							condition: 'or'
						})}
						autoFocus={true}
					/>
				</FilterContentContainer>
			}
		</Filter>
	)
}

export default LanguageFilter