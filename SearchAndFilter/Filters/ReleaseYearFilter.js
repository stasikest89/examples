import React from 'react'
import _ from 'lodash'
import FilterContentContainer from '../FilterContentContainer'
import Filter from './Filter'
import SelectWithInputTags from '../SelectWithInputTags'
import { useStore } from '../store/store'
import { TypeReleaseYear } from '../filterTypes'
import { ADD_SELECTION, DELETE_SELECTION, SET_CONDITION, SET_CONDITION_CLEAR_ITEMS, updateState } from '../src/handleUpdateState'
import { setReleaseYear } from '../store/reducer'

const ReleaseYearFilter = props => {
	const [state] = useStore()
	const [, dispatch] = useStore()
	
	const conditionIs = 'is'
	const conditionFrom = 'from'

	const {
		activeFilter,
		releaseYear = {}
	} = state

	const {
		items = [],
		condition
	} = releaseYear

	const active = activeFilter == TypeReleaseYear

	const changeProps = {
		state,
		type: TypeReleaseYear,
		dispatch,
		reducer: setReleaseYear
	}

	let displayInput = true
	if(condition == conditionFrom) {
		displayInput = items.length >= 2 ? false : true
	}

	return (
		<Filter {...props}>
			{active &&
				<FilterContentContainer type={TypeReleaseYear}>
					<SelectWithInputTags 
						overflow={false}
						selectedItem={condition}
						displayInput={displayInput}
						selectItems={[
							conditionIs,
							conditionFrom
						]}
						items={items}
						placeholder="Enter year/s"
						handleConditionSelection={condition => updateState({
							...changeProps,
							condition,
							action: condition == conditionFrom ? SET_CONDITION_CLEAR_ITEMS : SET_CONDITION
						})}
						handleSelection={item => {
							updateState({
								item,
								...changeProps,
								action: ADD_SELECTION
							})
						}}
						handleDelete={item => {
							updateState({
								item,
								...changeProps,
								action: DELETE_SELECTION
							})
						}}
						handleClear={() => {
							dispatch(setReleaseYear(initialState[TypeReleaseYear]))
						}}
						labelStyles={{
							margin: '0 0.34375rem 0.34375rem 0'
						}}
						autoFocus={true}
					/>
				</FilterContentContainer>
			}
		</Filter>
	)
}

export default ReleaseYearFilter