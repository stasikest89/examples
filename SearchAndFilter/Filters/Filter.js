import React, { useRef, useMemo } from 'react'
import _ from 'lodash'
import Settings from '../../Settings'
import { useStore } from '../store/store'
import { setActiveFilter, initialState } from '../store/reducer'

const Filter = props => {
	const {
		title,
		styles = {},
		children,
		type,
		reducer
	} = props

	const [, dispatch] = useStore()
	const [state] = useStore()

	const {
		activeFilter
	} = state

	const filterItems = state[type] || {}

	let {
		items = [],
		condition,
		conditionSingural,
		postFix,
		prefix,
		prefixCondition,
		joiningCondition,
		joiningConditions
	} = filterItems

	let headerStyles = {}
	let text = title
	let showDelete = false

	if(joiningConditions && joiningConditions[condition]) {
		joiningCondition = joiningConditions[condition]
	}

	if(!_.isEmpty(items) && Array.isArray(items)) {
		text = items.join(` ${joiningCondition || condition} `)
		showDelete = true
		headerStyles.background = "#4d4d4d"

		if(conditionSingural) {
			text = `${joiningCondition || condition} ${items.join(``)}`
		}

		if(postFix) {
			text = items.map(item => `${item}${postFix} `)
		}

		if(prefix) {
			text = `${prefix} ${text}`
		}

		if(prefixCondition) {
			text = `${condition} ${text}`
		}
	}

	return (
		<div 
			style={styles}
			id={`filter--${type}`}
			className="search-and-filter--filter">

			<div 
				className="header"	
				style={headerStyles}		
				onClick={() => dispatch(setActiveFilter(
				activeFilter == type ? "" : type))}>
					
				{text}

				{showDelete &&
					<img
						src={`${Settings.images.path}/svg/clear-white.svg`}
						onClick={e => {
							dispatch(reducer(initialState[type]))
							dispatch(setActiveFilter(initialState.activeFilter))
							e.stopPropagation()
						}}
						className="clear-filter"
					/>
				}
			</div>

			{children}
		</div>
	)
}

export default Filter