import React, { useState } from 'react'
import _ from 'lodash'
import ConnectSearchFilter from '../../SearchBox/ConnectSearchFilter'
import { useStore } from '../store/store'
import { initialState, setActiveFilter, setTitleSearch } from '../store/reducer'
import { TypeSearch } from '../filterTypes'
import ConnectCategories from '../../ConnectCategories'
import StringUnderlineSearch from '../../Helpers/StringUnderlineSearch'

const SearchFilter = props => {
	const {
		type,
		styles = {},
		state = {}
	} = props

	const {
		handleFetchCategoryRecordsWithRights,
		period,
		selectedCategory
	} = state

	const [, dispatch] = useStore()
	const [searchAndFilterState] = useStore()
	const [active, setActive] = useState(false)
	const [list, setList] = useState([])
	const [searchQuery, setSearchQuery] = useState("")
	
	const {
		titleSearch
	} = searchAndFilterState

	const category = ConnectCategories({
		selectedCategory
	})

	const handleSearch = search => {
		setList([])
		setSearchQuery(search)

		if(!search) return

		handleFetchCategoryRecordsWithRights({
			period,
			category,
			searchQuery: search,
			onSuccess: data => {
				if(Array.isArray(data)) {
					setList(
						data.map(item => {
							const value = item.original_title || ""

							return {
								text: StringUnderlineSearch(value, search),
								value
							}
						})
					)	
				}
			},
			onError: error => {
				console.log(error)
			}
		})
	}

	return (
		<div 
			style={styles}
			className="search-and-filter--filter">

			<ConnectSearchFilter 
				selected={titleSearch}
				searchQuery={searchQuery}
				handleSearch={handleSearch}
				setActive={setActive}
				onFocus={() => {
					dispatch(setActiveFilter(type))
				}}
				list={list}
				handleSelection={item => {
					setList([])
					dispatch(setTitleSearch(item.value))
				}}
				handleClear={() => dispatch(setTitleSearch(initialState[TypeSearch]))}
			/>
		</div>
	)
}

export default SearchFilter