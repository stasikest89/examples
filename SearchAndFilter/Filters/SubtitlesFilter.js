import React from 'react'
import { TypeSubtitles } from '../filterTypes'
import LanguageFilter from './LanguageFilter'
import { setSubtitles } from '../store/reducer'

const SubtitlesFilter = props => {
	return (
		<LanguageFilter 
			{...props}
			languageType={TypeSubtitles}
			reducer={setSubtitles}
		/>
	)
}

export default SubtitlesFilter