import React, { useMemo } from 'react'
import _ from 'lodash'
import FilterContentContainer from '../FilterContentContainer'
import Filter from './Filter'
import { useStore } from '../store/store'
import { TypeCertifications } from '../filterTypes'
import AndOrCheckboxes from '../AndOrCheckboxes'
import getUSARatings from '../../Options/getUSARatings'
import Checkbox from '../../Input/checkbox'
import { ADD_OR_REMOVE_SELECTION, SET_CONDITION, updateState } from '../src/handleUpdateState'
import { setCertifications } from '../store/reducer'

const CertificationsFilter = props => {
	const {
		options = {}
	} = props

	const [state] = useStore()
	const [, dispatch] = useStore()

	const {
		activeFilter,
		certifications = {}
	} = state

	const {
		items = [],
		condition
	} = certifications

	const ratings = useMemo(() => getUSARatings(options))
	const active = activeFilter == TypeCertifications

	const changeProps = {
		state,
		type: TypeCertifications,
		dispatch,
		reducer: setCertifications
	}

	return (
		<Filter {...props}>
			{active &&
				<FilterContentContainer 
					type={TypeCertifications}
					styles={{ minWidth: '10.625rem' }}>

					<AndOrCheckboxes 
						andStyle={{ margin: 0 }}
						condition={condition}
						handleAndClick={() => updateState({
							...changeProps,
							action: SET_CONDITION,
							condition: 'and'
						})}
						handleOrClick={() => updateState({
							...changeProps,
							action: SET_CONDITION,
							condition: 'or'
						})}
					/>

					<div className="vertical-checkboxes certifications-checkboxes">
						{ratings.map((item, index) => (
							<Checkbox
								checked={items.includes(item)}
								label={item}
								customCheckbox={true}
								className="connect-checkbox--container"
								customCheckboxClass="connect--checkbox"
								key={index}
								handleChange={() => updateState({
									item,
									...changeProps,
									action: ADD_OR_REMOVE_SELECTION
								})}
							/>
						))}
					</div>
				</FilterContentContainer>
			}
		</Filter>
	)
}

export default CertificationsFilter