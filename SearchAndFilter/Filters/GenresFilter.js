import React, { useState } from 'react'
import _ from 'lodash'
import FilterContentContainer from '../FilterContentContainer'
import Filter from './Filter'
import ConnectSearchWithLabels from '../../SearchBox/ConnectSearchWithLabels'
import { useStore } from '../store/store'
import { initialState, setGenres } from '../store/reducer'
import { TypeGenres } from '../filterTypes'
import ClientManagedDescriptors from "../../ClientManagedOptions/descriptors"
import ConnectCategories from '../../ConnectCategories'
import { ADD_SELECTION, DELETE_SELECTION, SET_CONDITION, updateState } from '../src/handleUpdateState'
import StringUnderlineSearch from '../../Helpers/StringUnderlineSearch'

const GenresFilter = props => {
	const {
		options = {},
		state = {}
	} = props 

	const [searchAndFilterState] = useStore()
	const [, dispatch] = useStore()

	const {
		activeFilter,
		genres = {},
	} = searchAndFilterState

	const {
		items = [],
		condition
	} = genres

	const {
		cmsData = {},
		selectedCategory
	} = state

	const {
		clientFeatures = {},
		clientDataStructure = {}
	} = cmsData

	const active = activeFilter == TypeGenres
	const [searchQuery, setSearchQuery] = useState("")

	const category = ConnectCategories({
		selectedCategory
	})

	const clientManagedGenresList = ClientManagedDescriptors({
		options,
		fallback: options.genres,
		clientFeatures,
		managed_lists: clientDataStructure.managed_lists || [],
		itemKey: 'genres',
		category
	})

	const [list, setList] = useState(clientManagedGenresList.map(item => {
		return {
			text: item.text,
			value: item.text
		}
	}))

	const handleSearch = search => {
		setSearchQuery(search)

		setList(
			clientManagedGenresList
			.filter(item => 
				item.text && 
				!item.text.toLowerCase().search(search.toLowerCase())
			)
			.map(item => {
				return {
					text: StringUnderlineSearch(item.text, search),
					value: item.text
				}
			})
		)
	}

	const changeProps = {
		state: searchAndFilterState,
		type: TypeGenres,
		dispatch,
		reducer: setGenres
	}

	return (
		<Filter {...props}>
			{active &&
				<FilterContentContainer type={TypeGenres}>
					<ConnectSearchWithLabels 
						handleSearch={handleSearch}
						placeholder="Select Genre"
						containerStyles={{
							width: '14.0625rem',
							border: 'none'
						}}
						list={list}
						items={items}
						condition={condition}
						searchQuery={searchQuery}
						displayAndOr={Array.isArray(items) && items.length > 1}
						handleSelection={item => {
							setSearchQuery("")
							updateState({
								item,
								...changeProps,
								action: ADD_SELECTION
							})
						}}
						handleDelete={item => updateState({
							item,
							...changeProps,
							action: DELETE_SELECTION
						})}
						handleClear={() => dispatch(setGenres(initialState[TypeGenres]))}
						handleAndClick={() => updateState({
							...changeProps,
							action: SET_CONDITION,
							condition: 'and'
						})}
						handleOrClick={() => updateState({
							...changeProps,
							action: SET_CONDITION,
							condition: 'or'
						})}
						autoFocus={true}
					/>
				</FilterContentContainer>
			}
		</Filter>
	)
}

export default GenresFilter