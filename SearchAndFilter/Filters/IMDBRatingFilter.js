import React, { useState, useMemo } from 'react'
import _ from 'lodash'
import FilterContentContainer from '../FilterContentContainer'
import Filter from './Filter'
import { useStore } from '../store/store'
import { TypeIMDBRating } from '../filterTypes'
import Checkbox from '../../Input/checkbox'
import { ADD_AND_REPLACE_SELECTION, updateState } from '../src/handleUpdateState'
import { setIMDBRating } from '../store/reducer'

const IMDBRatingFilter = props => {
	const [state] = useStore()
	const [, dispatch] = useStore()

	const {
		activeFilter,
		imdbRating = {}
	} = state

	const {
		items = []
	} = imdbRating

	const active = activeFilter == TypeIMDBRating

	const options = useMemo(() => {
		let result = []

		for (let i = 1; i < 11; i++) { 
			result.push(i)
		}

		return result
	})

	const changeProps = {
		state,
		type: TypeIMDBRating,
		dispatch,
		reducer: setIMDBRating
	}

	return (
		<Filter {...props}>
			{active &&
				<FilterContentContainer 
					type={TypeIMDBRating}
					styles={{ width: '5rem' }}>

					<div className="vertical-checkboxes">
						{options.map((item, index) => (
							<Checkbox
								checked={items.includes(item)}
								label={`${item} +`}
								customCheckbox={true}
								className="connect-checkbox--container"
								customCheckboxClass="connect--checkbox"
								key={index}
								handleChange={() => updateState({
									item,
									...changeProps,
									action: ADD_AND_REPLACE_SELECTION
								})}
							/>
						))}
					</div>
				</FilterContentContainer>
			}
		</Filter>
	)
}

export default IMDBRatingFilter