import React from 'react'
import { TypeAudioLanguages } from '../filterTypes'
import LanguageFilter from './LanguageFilter'
import { setAudioLanguages } from '../store/reducer'

const AudioLanguagesFilter = props => {
	return (
		<LanguageFilter 
			{...props}
			languageType={TypeAudioLanguages}
			reducer={setAudioLanguages}
			spoken={true}
		/>
	)
}

export default AudioLanguagesFilter