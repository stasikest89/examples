import React, { useState } from 'react'
import _ from 'lodash'
import Settings from '../Settings'
import SearchItems from './SearchItems'
import FilterContentContainer from './FilterContentContainer'
import Label from './Label'
import ConnectSearchFilter from '../SearchBox/ConnectSearchFilter'
import './styles/_select-with-input.scss'

const SelectWithInputTags = props => {
	const {
		selectedItem,
		selectItems = [],
		items = [],
		handleConditionSelection,
		handleSelection = () => {},
		handleDelete = () => {},
		displayInput = true,
		labelStyles = {},
		overflow
	} = props

	const [open, setOpen] = useState(false)
	
	return (
		<div className="select-with-input-tags">
			<div 
				className="select"
				onClick={() => setOpen(!open)}>

				<span>
					{selectedItem}
				</span>

				<img src={`${Settings.images.path}/svg/arrow-light-grey.svg`} />

				{open &&
					<FilterContentContainer 
						overflow={overflow}
						styles={{ padding: '0.625rem 0' }}>

						<SearchItems 
							list={selectItems}
							handleSelection={handleConditionSelection}
						/>
					</FilterContentContainer>
				}
			</div>

			{!_.isEmpty(items) &&
                items.map((item, index) => (
                    <Label 
                        title={item}
                        handleDelete={() => handleDelete(item)}
						deleteBlack={true}
						styles={labelStyles}
                        key={index}
                    />
                ))
            }

			{displayInput &&
				<ConnectSearchFilter 
					{...props}
					leftIcon={false}
					clearIcon={false}
					containerStyles={{
						width: '8rem',
						border: 0
					}}
					useEnterButtonOnSelect={true}
					handleEnter={handleSelection}
					key={items.join('-')}
				/>
			}
		</div>
	)
}

export default SelectWithInputTags