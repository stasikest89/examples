import React from 'react'
import { TypeSearch, TypeGenres, TypeReleaseYear, TypeIMDBRating, TypeCertifications, TypeAudioLanguages, TypeSubtitles } from './filterTypes'
import SearchFilter from './Filters/SearchFilter'
import GenresFilter from './Filters/GenresFilter'
import ReleaseYearFilter from './Filters/ReleaseYearFilter'
import IMDBRatingFilter from './Filters/IMDBRatingFilter'
import CertificationsFilter from './Filters/CertificationsFilter'
import AudioLanguagesFilter from './Filters/AudioLanguagesFilter'
import SubtitlesFilter from './Filters/SubtitlesFilter'
import { setTitleSearch, setGenres, setReleaseYear, setCertifications, setAudioLanguages, setSubtitles, setIMDBRating } from './store/reducer'

export default function filters() {    
    return {
        titleSearch: {
            title: "Title Search",
            type: TypeSearch,
            renderContent: props => <SearchFilter {...props}/>,
            reducer: state => setTitleSearch(state)
        },
        genres: {
            title: "Genres",
            type: TypeGenres,
            renderContent: props => <GenresFilter {...props}/>,
            reducer: state => setGenres(state)
        },
        releaseYear: {
            title: "Release Year",
            type: TypeReleaseYear,
            renderContent: props => <ReleaseYearFilter {...props}/>,
            reducer: state => setReleaseYear(state)
        },
        certifications: {
            title: "Certifications",
            type: TypeCertifications,
            renderContent: props => <CertificationsFilter {...props}/>,
            reducer: state => setCertifications(state)
        },
        imdbRating: {
            title: "IMDB Rating",
            type: TypeIMDBRating,
            renderContent: props => <IMDBRatingFilter {...props}/>,
            reducer: state => setIMDBRating(state)
        },
        audioLanguages: {
            title: "Audio Languages",
            type: TypeAudioLanguages,
            renderContent: props => <AudioLanguagesFilter {...props}/>,
            reducer: state => setAudioLanguages(state)
        },
        subtitles: {
            title: "Subtitles",
            type: TypeSubtitles,
            renderContent: props => <SubtitlesFilter {...props}/>,
            reducer: state => setSubtitles(state)
        }
    }
}