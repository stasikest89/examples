import React from 'react'
import './styles/_styles.scss'
import Button from '../Button'
import { useStore } from './store/store'
import { setDefaultState } from './store/reducer'

const SearchAndFilter = props => {
	const {
		filters = [],
		state = {}
	} = props

	const {
		options = {},
		cmsData = {}
	} = state

	const [searchAndFilterState] = useStore()
	const [, dispatch] = useStore()

	const {
		filtersApplied,
		viewKey
	} = searchAndFilterState

	return (
		<div 
			className="search-and-filter" 
			key={viewKey}>
				
			<div className="search-and-filter--content">
				{filters.map((filter, index) => {
					const {
						renderContent
					} = filter

					const itemProps = {
						key: index,
						...filter,
						options,
						state,
						cmsData
					}

					return renderContent(itemProps)
				})}
			</div>

			{filtersApplied &&
				<Button
					value="Clear All"
					orange={true}
					margin="0.34375rem 0 0"
					onClick={() => dispatch(setDefaultState())}
					fontWeight="normal"
					fontSize="0.875rem"
					textTransform="capitalize"
					styles={{
						height: '2.5rem',
						padding: '0 0.875rem',
						width: '6.5rem'
					}}
				/>
			}
		</div>
	)
}

export default SearchAndFilter