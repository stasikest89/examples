import React from 'react'
import KeywordLabel from '../KeywordLabel'

const Label = props => {
    return (
        <KeywordLabel
            {...props}
            margin="0 0.34375rem 0 0"
            delete={true}
            className="search-and-filter--label"
        />
    )
}

export default Label