import React, { useMemo } from 'react'
import SearchAndFilter from './index'
import './styles/_connect.scss'
import getFilters from './filters'

const ConnectSearchAndFilter = props => {
	const filters = useMemo(() => getFilters())
	const connectFilters = useMemo(() => {
		return [
			filters.titleSearch,
			filters.genres,
			filters.releaseYear,
			filters.certifications,
			filters.imdbRating,
			filters.audioLanguages,
			filters.subtitles
		]
	})

	return (
		<div>
			<SearchAndFilter 
				{...props}
				filters={connectFilters}
			/>
		</div>
	)
}

export default ConnectSearchAndFilter