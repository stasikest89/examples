import React from 'react'
import Checkbox from '../Input/checkbox'

const AndOrCheckboxes = props => {
	const {
		andStyle = {},
		handleAndClick = () => {},
		handleOrClick = () => {},
		condition
	} = props 

	return (
		<div className="search-and-filter--checkboxes">
			<Checkbox
				checked={condition == 'and'}
				label="And"
				className="connect-checkbox--container"
				customCheckbox={true}
				customCheckboxClass="connect--checkbox"
				style={andStyle}
				handleChange={handleAndClick}
			/>

			<Checkbox
				checked={condition == 'or'}
				label="Or"
				className="connect-checkbox--container"
				customCheckbox={true}
				customCheckboxClass="connect--checkbox"
				handleChange={handleOrClick}
			/>
		</div>
	)
}

export default AndOrCheckboxes