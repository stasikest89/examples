export const SET_SEARCH_QUERY = 'SET_SEARCH_QUERY'
export const ADD_SELECTION = 'ADD_SELECTION'
export const DELETE_SELECTION = 'DELETE_SELECTION'
export const SET_CONDITION = 'SET_CONDITION'
export const ADD_OR_REMOVE_SELECTION = 'ADD_OR_REMOVE_SELECTION'
export const ADD_AND_REPLACE_SELECTION = 'ADD_AND_REPLACE_SELECTION'
export const SET_CONDITION_CLEAR_ITEMS = 'SET_CONDITION_CLEAR_ITEMS'

export function updateState(props) {
	const {
		item = {},
		state = {},
		type,
		dispatch,
		reducer,
		action
	} = props

	const typeState = state[type] || {}
	let updatedState = [...typeState.items || []]

	const itemToUse = getItemToUse({
		item,
		typeState
	})

	if(action == ADD_SELECTION) {
		updatedState = handleAddSelection({
			updatedState,
			itemToUse
		})
	}

	if(action == ADD_AND_REPLACE_SELECTION) {
		updatedState = handleAddAndReplaceSelection({
			updatedState,
			itemToUse
		})
	}

	if(action == DELETE_SELECTION) {
		updatedState = handleDeleteSelection({
			updatedState,
			item
		})
	}

	if(action == ADD_OR_REMOVE_SELECTION) {
		updatedState = handleAddOrRemoveSelection({
			updatedState,
			itemToUse
		})
	}

	if(action == SET_CONDITION) {
		return handleSetCondition({
			typeState,
			...props
		})
	}

	if(action == SET_CONDITION_CLEAR_ITEMS) {
		return handleSetConditionClearItems({
			typeState,
			...props
		})
	}

	if(action == SET_SEARCH_QUERY) {
		return handleSetSearchQuery({
			typeState,
			...props
		})
	}

	dispatch(reducer({
		...typeState,
		items: updatedState,
	}))
}	

function getItemToUse(props) {
	const {
		item,
		typeState
	} = props

	const {
		useValue
	} = typeState

	if(useValue) {
		return item.value
	}

	return item.text || item
}

function handleAddSelection(props) {
	let {
		updatedState,
		itemToUse
	} = props 

	if(updatedState.includes(itemToUse)) {
		return updatedState
	}

	updatedState.push(itemToUse)

	return updatedState
}

function handleAddAndReplaceSelection(props) {
	let {
		itemToUse,
		updatedState
	} = props 

	if(updatedState.includes(itemToUse)) {
		return []
	}

	return [itemToUse]
}

function handleAddOrRemoveSelection(props) {
	let {
		updatedState,
		itemToUse
	} = props 

	if(updatedState.includes(itemToUse)) {
		return handleDeleteSelection({
			...props,
			item: itemToUse
		})
	} else {
		return handleAddSelection(props)
	}
}

function handleDeleteSelection(props) {
	let {
		updatedState,
		item,
	} = props 

	if(updatedState.includes(item)) {
		updatedState = updatedState.filter(us => us !== item) 
	}

	return updatedState
}

function handleSetCondition(props) {
	const {
		typeState = {},
		condition,
		dispatch,
		reducer
	} = props

	dispatch(reducer({
		...typeState,
		condition
	}))
}

function handleSetConditionClearItems(props) {
	const {
		typeState = {},
		condition,
		dispatch,
		reducer
	} = props

	dispatch(reducer({
		...typeState,
		condition,
		items: []
	}))
}

function handleSetSearchQuery(props) {
	const {
		typeState = {},
		searchQuery,
		dispatch,
		reducer
	} = props

	dispatch(reducer({
		...typeState,
		searchQuery
	}))
}