import React from 'react'

const SearchItems = props => {
	const {
		list = [],
		handleSelection = () => {}
	} = props
	
	return (
		<div className="search--items">
			{list.map((item, index) => (
				<div 
					key={index} 
					onClick={() => handleSelection(item)}> 

					<span dangerouslySetInnerHTML={{
						__html: item.text || item
					}}/>
				</div>
			))}
		</div>
	)
}

export default SearchItems